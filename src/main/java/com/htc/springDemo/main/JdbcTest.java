package com.htc.springDemo.main;

import java.util.List;

//package com.htc.spring5demo.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.htc.springDemo.config.SpringAppConfig;
import com.htc.springDemo.dao.ProductDAO;
import com.htc.springDemo.model.*;

public class JdbcTest {

	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringAppConfig.class);
		ProductDAO dao= (ProductDAO)context.getBean("productDAO");
	
		//boolean result =dao.addProduct(new Product("p0008", "Printer", "Input device", 5000.0, 5));
		//System.out.println(result);
		
//		boolean result =dao.addProduct(new Product("p0004", "Mouse", "Input device", 500.0, 3));
//		System.out.println(result);
		
//		boolean result =dao.addProduct(new Product("p0002", "Keyboard", "Input device", 900.0, 4));\
//		System.out.println(result);
		
//		boolean result =dao.addProduct(new Product("p0007", "Bingo", "stationary", 900.0, 4));
//		System.out.println(result);
//		
		//boolean result =dao.removeProduct("p0006");
		//System.out.println(result);
		
//	    Product result=dao.getProduct("p0004");
//		System.out.println(result);
		
//		List<Product> result=dao.getProducts();
//		System.out.println(result);
		
//		boolean result=dao.updateProduct("P0002", 4763.0, 4);
//		System.out.println(result);
		
//		boolean result= dao.updateProduct("p0008", 50.0, 6);
//		System.out.println(result);
		
		List<Product> result=dao.getProducts("Input device");
   	System.out.println(result);
		
	}
}
