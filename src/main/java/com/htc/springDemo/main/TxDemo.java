package com.htc.springDemo.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.htc.springDemo.config.SpringAppConfig;
import com.htc.springDemo.service.ProductService;

public class TxDemo {

	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringAppConfig.class);

		ProductService productService = (ProductService)context.getBean("productService");
		boolean result = productService.removeCategory("Output device");
		System.out.println(result);
	}
}
