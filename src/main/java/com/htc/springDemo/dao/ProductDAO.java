package com.htc.springDemo.dao;

//package com.htc.spring5demo.dao;

import java.util.List;

import com.htc.springDemo.model.Product;

public interface ProductDAO {

	public boolean addProduct(Product p);
	public Product getProduct(String productCode);
	public List<Product> getProducts(String category);
	public List<Product> getProducts();
	public boolean removeProduct(String productCode);
	public boolean removeProducts(String category);
	public boolean updateProduct(String productCode, double newPrice, int qoh);
}
