package com.htc.springDemo.dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.htc.springDemo.model.Category;

public class CategoryDAOImpl implements CategoryDAO{

	JdbcTemplate jdbcTemplate;
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public boolean addCategory(Category category) {
		boolean addStatus = false;
		int result = jdbcTemplate.update("insert into category values(?,?)", category.getCategory(), category.getCategoryDesc());
		if(result ==1 )
			addStatus =true;
		
		return addStatus;
		
	}
	public Category getCategory(String category) {
		Category cat = null;
		cat = jdbcTemplate.queryForObject("select category, catdesc from categories where category = ?",
				new RowMapper<Category>() {
					public Category mapRow(ResultSet rs, int rownum) throws SQLException {
						System.out.println(rownum);
						Category c = new Category();
						c.setCategory(rs.getString(1));
						c.setCategoryDesc(rs.getString(2));
						return c;
					}
				}, 
				category);
		return cat;

	}

	public boolean removeCategory(String category) {
		boolean deleteStatus = false;
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);
		
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("category", category);
		
		int result = template.update("delete from categories where category=:category", paramMap);
		
		if(result == 1)
			deleteStatus = true;
		
		return deleteStatus;

	}
}
