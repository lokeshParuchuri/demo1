package com.htc.springDemo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.htc.springDemo.model.Product;

public class ProductDAOImpl implements ProductDAO {

	JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public boolean addProduct(Product p) {
		boolean addStatus = false;
		int result = jdbcTemplate.update("insert into products values(?,?,?,?,?)", p.getProdutCode(),
				p.getProductDescription(), p.getCategory(), p.getUnitPrice(), p.getQoh());
		if (result == 1)
			addStatus = true;

		return addStatus;
	}

	public Product getProduct(String productCode) {
		Product product = null;
		product = jdbcTemplate.queryForObject(
				"select productCode, productDescription, category, unitPrice , qoh from products where productCode = ?",
				new RowMapper<Product>() {
					public Product mapRow(ResultSet rs, int rownum) throws SQLException {
						System.out.println(rownum);
						Product p = new Product();
						p.setProdutCode(rs.getString(1));
						p.setProductDescription(rs.getString(2));
						p.setCategory(rs.getString(3));
						p.setUnitPrice(rs.getDouble(4));
						p.setQoh(rs.getInt(5));
						return p;
					}
				}, productCode);
		return product;
	}

	public List<Product> getProducts(String category) {
		List<Product> prodList = this.jdbcTemplate.query(
				"select productCode,productDescription,category,unitPrice,qoh from products where category=?",
				new RowMapper<Product>() {
					public Product mapRow(ResultSet rs, int rownum) throws SQLException {
						System.out.println(rownum);
						Product p = new Product();
						p.setProdutCode(rs.getString(1));
						p.setProductDescription(rs.getString(2));
						p.setCategory(rs.getString(3));
						p.setUnitPrice(rs.getDouble(4));
						p.setQoh(rs.getInt(5));
						return p;

					}
				}, category);
		return prodList;
	}

	public List<Product> getProducts() {
		// TODO Auto-generated method stub
		return jdbcTemplate.query("select * from products", new ResultSetExtractor<List<Product>>() {
			public List<Product> extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<Product> list = new ArrayList<Product>();
				while (rs.next()) {
					Product e = new Product();
					e.setProdutCode(rs.getString(1));
					e.setProductDescription(rs.getString(2));
					e.setCategory(rs.getString(3));
					e.setUnitPrice(rs.getInt(4));
					e.setQoh(rs.getInt(5));
					list.add(e);
				}
				return list;
			}
		});
	}
	// return null;
	public boolean removeProduct(String productCode) {
		// TODO Auto-generated method stub
		boolean addStatus = false;
		int result = jdbcTemplate.update("delete from products where productcode=?", productCode);
		if (result == 1)
			addStatus = true;

		return addStatus;
		// return false;

//		public boolean removeProduct(String productCode) {
//			boolean deleteStatus = false;
//			NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);
//			
//			Map<String, String> paramMap = new HashMap<String, String>();
//			paramMap.put("pcode", productCode);
//			
//			int result = template.update("delete from products where productCode=:pcode", paramMap);
//			
//			if(result == 1)
//				deleteStatus = true;
//			
//			return deleteStatus;
//		}

	}

	public boolean removeProducts(String category) {
		boolean deleteStatus = false;
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("category", category);

		int result = template.update("delete from products where category=:category", paramMap);

		if (result > 1)
			deleteStatus = true;

		return deleteStatus;
	}

	public boolean updateProduct(String productCode, double newPrice, int qoh) {
//		boolean updateStatus = false;
//		int result = jdbcTemplate.update("update products set unitPrice=?,qoh=? where productcode=?",newPrice,qoh,productCode);
//		if (result == 1)
//			updateStatus = true;
//
//		return updateStatus;
		// return false;

		// update products set unitPrice = :newPrice, qoh=:qoh where productCode=:pcode
		boolean updateStatus = false;
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);

		Map paramMap = new HashMap();
		paramMap.put("pcode", productCode);
		paramMap.put("punitPrice", newPrice);
		paramMap.put("pqoh", qoh);
		int result = template.update("update products set unitPrice=:punitPrice,qoh=:pqoh where productcode=:pcode",
				paramMap);

		if (result == 1)
			updateStatus = true;

		return updateStatus;
//	}
	}

}
