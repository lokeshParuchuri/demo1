package com.htc.springDemo.model;

//package com.htc.spring5demo.model;

public class Product {

	private String produtCode;
	private String productDescription;
	private String category;
	private double unitPrice;
	private int qoh;
	
	public Product() {}

	public Product(String produtCode, String productDescription, String category, double unitPrice, int qoh) {
		super();
		this.produtCode = produtCode;
		this.productDescription = productDescription;
		this.category = category;
		this.unitPrice = unitPrice;
		this.qoh = qoh;
	}

	public String getProdutCode() {
		return produtCode;
	}

	public void setProdutCode(String produtCode) {
		this.produtCode = produtCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getQoh() {
		return qoh;
	}

	public void setQoh(int qoh) {
		this.qoh = qoh;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((productDescription == null) ? 0 : productDescription.hashCode());
		result = prime * result + ((produtCode == null) ? 0 : produtCode.hashCode());
		result = prime * result + qoh;
		long temp;
		temp = Double.doubleToLongBits(unitPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (productDescription == null) {
			if (other.productDescription != null)
				return false;
		} else if (!productDescription.equals(other.productDescription))
			return false;
		if (produtCode == null) {
			if (other.produtCode != null)
				return false;
		} else if (!produtCode.equals(other.produtCode))
			return false;
		if (qoh != other.qoh)
			return false;
		if (Double.doubleToLongBits(unitPrice) != Double.doubleToLongBits(other.unitPrice))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Product [produtCode=" + produtCode + ", productDescription=" + productDescription + ", category="
				+ category + ", unitPrice=" + unitPrice + ", qoh=" + qoh + "]";
	}
	
}
