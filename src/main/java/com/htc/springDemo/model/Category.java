package com.htc.springDemo.model;

public class Category {

	private String category;
	private String categoryDesc;

	public Category() {
	}

	public Category(String category, String categoryDesc) {
		this.category = category;
		this.categoryDesc = categoryDesc;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategoryDesc() {
		return categoryDesc;
	}

	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}

	@Override
	public String toString() {
		return "Category [category=" + category + ", categoryDesc=" + categoryDesc + "]";
	}

}
