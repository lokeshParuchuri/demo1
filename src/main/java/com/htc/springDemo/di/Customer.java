package com.htc.springDemo.di;

//package com.htc.spring5demo.di;

public class Customer {

	private String customerCode;
	private String customerName;
	private Address address;
	private String contactNo;
	
	public Customer() {}

	public Customer(String customerCode, String customerName, Address address, String contactNo) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactNo = contactNo;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	@Override
	public String toString() {
		return "Customer [customerCode=" + customerCode + ", customerName=" + customerName + ", contactNo=" + contactNo
				+ "]";
	}


}
